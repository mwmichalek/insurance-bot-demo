﻿// Copyright (c) Microsoft Corporation. All rights reserved.
// Licensed under the MIT License.

using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using AdaptiveCards;
using Microsoft.Bot.Builder;
using Microsoft.Bot.Builder.Dialogs;
using Microsoft.Bot.Builder.Dialogs.Choices;
using Microsoft.Bot.Schema;
using MultiTurnPromptsBot;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

namespace Microsoft.BotBuilderSamples {
    public class InsuranceBot : IBot {
        private const string WelcomeText = "Hi! I've got just a few questions to help quote the best plan for your needs.";

        private readonly InsuranceBotAccessors _accessors;

        private DialogSet _dialogs;

        public InsuranceBot(InsuranceBotAccessors accessors) {
            _accessors = accessors ?? throw new ArgumentNullException(nameof(accessors));

            _dialogs = new DialogSet(accessors.ConversationDialogState);

            var readyAndInsuranceTypeWF = new WaterfallStep[] {
                AskReadyToStartAsync,
                HandleReadyToStart_AskInsuranceRequesteeTypeAsync,
                HandleInsuranceRequesteeType_AskMemberCountAsync,
                HandleMemberCount_AskZipCodeAsync,
            };

            var zipIncomeSummaryWF = new WaterfallStep[] {
                AskZipCodeAsync,
                HandleZipCode_AskHouseIncomeAsync,
                HandleHouseIncome_AskSummaryAsync
            };

            // Add named dialogs to the DialogSet. These names are saved in the dialog state.
            _dialogs.Add(new WaterfallDialog("readyAndInsuranceTypeWF", readyAndInsuranceTypeWF));
            _dialogs.Add(new WaterfallDialog("zipIncomeSummaryWF", zipIncomeSummaryWF));
            _dialogs.Add(new ChoicePrompt(nameof(AskReadyToStartAsync)));
            _dialogs.Add(new TextPrompt(nameof(HandleReadyToStart_AskInsuranceRequesteeTypeAsync)));
            _dialogs.Add(new TextPrompt(nameof(HandleInsuranceRequesteeType_AskMemberCountAsync)));
            _dialogs.Add(new NumberPrompt<int>(nameof(HandleMemberCount_AskZipCodeAsync)));
            //_dialogs.Add(new NumberPrompt<int>(nameof(FamilyMemberCountAsync)));
            //_dialogs.Add(new NumberPrompt<int>(nameof(ChildCountAsync)));
            _dialogs.Add(new NumberPrompt<int>(nameof(HandleZipCode_AskHouseIncomeAsync)));
            _dialogs.Add(new ChoicePrompt(nameof(HandleHouseIncome_AskSummaryAsync)));
        }
        
        public async Task OnTurnAsync(ITurnContext turnContext, CancellationToken cancellationToken = default(CancellationToken)) {
            if (turnContext == null) throw new ArgumentNullException(nameof(turnContext));
            
            if (turnContext.Activity.Type == ActivityTypes.Message) {
                if (string.IsNullOrEmpty(turnContext.Activity.Text)) {
                    turnContext.Activity.Text = ((JObject)turnContext.Activity.Value).ToString();
                }

                var dialogContext = await _dialogs.CreateContextAsync(turnContext, cancellationToken);
                var results = await dialogContext.ContinueDialogAsync(cancellationToken);

                //if (results.Status == DialogTurnStatus.Empty) {
                //    await dialogContext.BeginDialogAsync("readyAndInsuranceTypeWF", null, cancellationToken);
                //}

            } else if (turnContext.Activity.Type == ActivityTypes.ConversationUpdate) {
                if (turnContext.Activity.MembersAdded != null) {
                    await SendWelcomeMessageAsync(turnContext, cancellationToken);
                }
            } else if (turnContext.Activity.Type == "connected") {
                //var reply = turnContext.Activity.CreateReply();
                //reply.Text = "I hear you, chilax";
                //await turnContext.SendActivityAsync(reply, cancellationToken);
                //await SendWelcomeMessageAsync(turnContext, cancellationToken);
            } else {
                await turnContext.SendActivityAsync($"{turnContext.Activity.Type} event detected", cancellationToken: cancellationToken);
            }

            await _accessors.ConversationState.SaveChangesAsync(turnContext, false, cancellationToken);

            await _accessors.UserState.SaveChangesAsync(turnContext, false, cancellationToken);
        }

        private async Task SendWelcomeMessageAsync(ITurnContext turnContext, CancellationToken cancellationToken) {
            foreach (var member in turnContext.Activity.MembersAdded) {
                if (member.Id != turnContext.Activity.Recipient.Id) {
                    var reply = turnContext.Activity.CreateReply();
                    reply.Text = WelcomeText;
                    await turnContext.SendActivityAsync(reply, cancellationToken);

                    var dialogContext = await _dialogs.CreateContextAsync(turnContext, cancellationToken);
                    var results = await dialogContext.ContinueDialogAsync(cancellationToken);

                    if (results.Status == DialogTurnStatus.Empty) {
                        await dialogContext.BeginDialogAsync("readyAndInsuranceTypeWF", null, cancellationToken);
                    }
                }
            }
        }

        private async Task<DialogTurnResult> AskReadyToStartAsync(WaterfallStepContext stepContext, CancellationToken cancellationToken) {
            return await stepContext.PromptAsync(nameof(AskReadyToStartAsync), ReadyToStartPromptOptions(), cancellationToken);
        }

        private PromptOptions ReadyToStartPromptOptions() {
            var choices = new List<string> {
                "Let's go!",
                "I prefer humans."
            };
            return new PromptOptions {
                Choices = ChoiceFactory.ToChoices(choices),
                Prompt = MessageFactory.Text("Ready to start?"),
                RetryPrompt = MessageFactory.Text("I'm sorry I didn't understand your responce, try typing 1 or 2.")
            };
        }

        private bool ProcessReadyToStartResult(WaterfallStepContext stepContext) {
            var doitornot = stepContext.Result as FoundChoice;
            return (doitornot != null && doitornot.Index == 0);
        }

        private async Task<DialogTurnResult> HandleReadyToStart_AskInsuranceRequesteeTypeAsync(WaterfallStepContext stepContext, CancellationToken cancellationToken) {
            if (ProcessReadyToStartResult(stepContext)) {
                return await stepContext.PromptAsync(nameof(HandleReadyToStart_AskInsuranceRequesteeTypeAsync),
                    CardPromptOptions(stepContext, @"./Resources/InsuranceRequesteeType.json"),
                    cancellationToken);
            } else {
                await stepContext.Context.SendActivityAsync(MessageFactory.Text($"Call us at 1-900-MIX-ALOT when you are ready."), cancellationToken);

                return await stepContext.EndDialogAsync(cancellationToken: cancellationToken);
            }
        }

        private PromptOptions CardPromptOptions(WaterfallStepContext stepContext, string templatePath) { 
            var cardAttachment = CreateAdaptiveCardAttachment(templatePath);
            var reply = stepContext.Context.Activity.CreateReply();
            reply.Attachments = new List<Attachment>() { cardAttachment };

            return new PromptOptions {
                Prompt = (Activity)MessageFactory.Attachment(new Attachment {
                    ContentType = AdaptiveCard.ContentType,
                    Content = cardAttachment.Content
                }),
            };
        }

        private async Task<InsuranceQuestionaire> ProcessInsuranceRequesteeTypeResultAsync(WaterfallStepContext stepContext, CancellationToken cancellationToken) {
            var insuranceRequesteeType = (string)stepContext.Result;
            var insuranceQuestionaire = await _accessors.InsuranceQuestionaire.GetAsync(stepContext.Context, () => new InsuranceQuestionaire(), cancellationToken);
            if (!string.IsNullOrEmpty(insuranceRequesteeType)) {
                insuranceQuestionaire.InsuranceRequesteeType = insuranceRequesteeType;

                if (insuranceQuestionaire.InsuranceRequesteeType.Contains("Me"))
                    insuranceQuestionaire.FamilyMemberCount = 1;
                if (insuranceQuestionaire.InsuranceRequesteeType.Contains("Spouse"))
                    insuranceQuestionaire.FamilyMemberCount = 2;
            }

            return insuranceQuestionaire;
        }

        private async Task<DialogTurnResult> HandleInsuranceRequesteeType_AskMemberCountAsync(WaterfallStepContext stepContext, CancellationToken cancellationToken) {
            var insuranceQuestionaire = await ProcessInsuranceRequesteeTypeResultAsync(stepContext, cancellationToken);

            if (insuranceQuestionaire.InsuranceRequesteeType == "Just Me" ||
                insuranceQuestionaire.InsuranceRequesteeType == "Me and Spouse") {

                await stepContext.EndDialogAsync(null, cancellationToken);
                return await stepContext.BeginDialogAsync("zipIncomeSummaryWF", null, cancellationToken);
            }

            return await stepContext.PromptAsync(nameof(HandleInsuranceRequesteeType_AskMemberCountAsync),
                CardPromptOptions(stepContext, @"./Resources/MemberCount.json"),
                cancellationToken);
        }

        private async Task<InsuranceQuestionaire> ProcessMemberCountResultAsync(WaterfallStepContext stepContext, CancellationToken cancellationToken) {
            var insuranceQuestionaire = await _accessors.InsuranceQuestionaire.GetAsync(stepContext.Context, () => new InsuranceQuestionaire(), cancellationToken);
            var childContsStr = (string)stepContext.Result;
            if (!string.IsNullOrEmpty(childContsStr)) {
                var childCountsJson = JObject.Parse(childContsStr);

                if (childCountsJson != null) {
                    insuranceQuestionaire.FamilyMemberCount += int.Parse((string)childCountsJson["bigKids"]);
                    insuranceQuestionaire.ChildCount = int.Parse((string)childCountsJson["smallKids"]);
                    insuranceQuestionaire.FamilyMemberCount += insuranceQuestionaire.ChildCount;
                }
            }
            return insuranceQuestionaire;
        }

        private async Task<DialogTurnResult> HandleMemberCount_AskZipCodeAsync(WaterfallStepContext stepContext, CancellationToken cancellationToken) {
            var insuranceQuestionaire = await ProcessMemberCountResultAsync(stepContext, cancellationToken);
            await stepContext.EndDialogAsync(null, cancellationToken);
            return await stepContext.BeginDialogAsync("zipIncomeSummaryWF", null, cancellationToken);
        }

        private async Task<DialogTurnResult> AskZipCodeAsync(WaterfallStepContext stepContext, CancellationToken cancellationToken) {
            return await stepContext.PromptAsync(nameof(HandleMemberCount_AskZipCodeAsync), ZipCodePromptOptions(), cancellationToken);
        }

        private PromptOptions ZipCodePromptOptions() {
            return new PromptOptions {
                Prompt = MessageFactory.Text("What's your zip code?"),
                RetryPrompt = MessageFactory.Text("I'm sorry I didn't understand your responce, we are expecting a 5 digit number.")
            };
        }



        private async Task ProcessZipCodeResultAsync(WaterfallStepContext stepContext, CancellationToken cancellationToken) {
            var zipCode = (int)stepContext.Result;
            var insuranceQuestionaire = await _accessors.InsuranceQuestionaire.GetAsync(stepContext.Context, () => new InsuranceQuestionaire(), cancellationToken);
            insuranceQuestionaire.ZipCode = zipCode;
        }

        //private async Task<DialogTurnResult> FamilyMemberCountAsync(WaterfallStepContext stepContext, CancellationToken cancellationToken) {
        //    await ProcessZipCodeResultAsync(stepContext, cancellationToken);
        //    return await stepContext.PromptAsync(nameof(FamilyMemberCountAsync), 
        //                                         new PromptOptions {
        //                                             Prompt = MessageFactory.Text("How many people live in your household, including you?"),
        //                                             RetryPrompt = MessageFactory.Text("I'm sorry I didn't understand your responce. we are expecting a 5 digit number.")
        //                                         }, cancellationToken);
        //}

        //private async Task<InsuranceQuestionaire> ProcessFamilyMemberCountResultAsync(WaterfallStepContext stepContext, CancellationToken cancellationToken) {
        //    var familyMemberCount = (int)stepContext.Result;
        //    var insuranceQuestionaire = await _accessors.InsuranceQuestionaire.GetAsync(stepContext.Context, () => new InsuranceQuestionaire(), cancellationToken);
        //    insuranceQuestionaire.FamilyMemberCount = familyMemberCount;
        //    return insuranceQuestionaire;
        //}

        //private async Task<DialogTurnResult> ChildCountAsync(WaterfallStepContext stepContext, CancellationToken cancellationToken) {
        //    await ProcessFamilyMemberCountResultAsync(stepContext, cancellationToken);
        //    return await stepContext.PromptAsync(nameof(ChildCountAsync), 
        //                                         new PromptOptions {
        //                                             Prompt = MessageFactory.Text("How many kids do you have under the age of 19?"),
        //                                             RetryPrompt = MessageFactory.Text("I'm sorry I didn't understand your responce. we are expecting a number.")
        //                                         }, cancellationToken);
        //}

        //private async Task ProcessChildCountResultAsync(WaterfallStepContext stepContext, CancellationToken cancellationToken) {
        //    var childCount = (int)stepContext.Result;
        //    var insuranceQuestionaire = await _accessors.InsuranceQuestionaire.GetAsync(stepContext.Context, () => new InsuranceQuestionaire(), cancellationToken);
        //    insuranceQuestionaire.ChildCount = childCount;
        //}

        private async Task<DialogTurnResult> HandleZipCode_AskHouseIncomeAsync(WaterfallStepContext stepContext, CancellationToken cancellationToken) {
            await ProcessZipCodeResultAsync(stepContext, cancellationToken);
            return await stepContext.PromptAsync(nameof(HandleZipCode_AskHouseIncomeAsync), 
                                                 new PromptOptions {
                                                     Prompt = MessageFactory.Text("What will your total household income be this year?"),
                                                     RetryPrompt = MessageFactory.Text("I'm sorry I didn't understand your responce. we are expecting a number.")
                                                 }, cancellationToken);
        }

        private async Task ProcessHouseIncomeResultAsync(WaterfallStepContext stepContext, CancellationToken cancellationToken) {
            var houseIncome = (int)stepContext.Result;
            var insuranceQuestionaire = await _accessors.InsuranceQuestionaire.GetAsync(stepContext.Context, () => new InsuranceQuestionaire(), cancellationToken);
            insuranceQuestionaire.HouseIncome = houseIncome;
        }

        private async Task<DialogTurnResult> HandleHouseIncome_AskSummaryAsync(WaterfallStepContext stepContext, CancellationToken cancellationToken) {
            await ProcessHouseIncomeResultAsync(stepContext, cancellationToken);

            await stepContext.Context.SendActivityAsync(MessageFactory.Text($"Judging from your answer, you can get this wicked sweet deal [Connect to service]."), cancellationToken);

            var choices = new List<string> {
                "Yes, I'll call you",
                "No thank you",
                "Call me",
            };

            return await stepContext.PromptAsync(nameof(HandleHouseIncome_AskSummaryAsync),
                                                    new PromptOptions {
                                                        Choices = ChoiceFactory.ToChoices(choices),
                                                        Prompt = MessageFactory.Text("Want to discuss further with one of our experts?"),
                                                        RetryPrompt = MessageFactory.Text("I'm sorry I didn't understand your responce, try typing a number.")
                                                    },
                                                    cancellationToken);
        }

        private static Attachment CreateAdaptiveCardAttachment(string filePath) {
            var adaptiveCardJson = File.ReadAllText(filePath);
            var adaptiveCardAttachment = new Attachment() {
                ContentType = "application/vnd.microsoft.card.adaptive",
                Content = JsonConvert.DeserializeObject(adaptiveCardJson),
            };
            return adaptiveCardAttachment;
        }
    }
}
